ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.7"

lazy val root = (project in file("."))
  .settings(
    name := "LabTaFi5",
    idePackagePrefix := Some("com.plyshka.tafi.lab5")
  )
