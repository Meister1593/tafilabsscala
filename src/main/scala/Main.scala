package com.plyshka.tafi.lab5

import scala.collection.mutable

object Main {
  def main(args: Array[String]): Unit = {
    val automaton = new Lab5
    val rules: Map[String, List[String]] = Map(
      "Q" -> List("01A", "01B", "A"),
      "A" -> List("0B1", "B", "1"),
      "B" -> List("BA0", "B1", "C"),
      "C" -> List("0C1", "1"),
      "D" -> List("D1", "0", "1")
    )

    automaton.simulateMagazine(new mutable.StringBuilder("011"), rules)
  }
}
