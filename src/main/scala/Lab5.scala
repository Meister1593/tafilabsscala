package com.plyshka.tafi.lab5

import scala.collection.mutable

class Lab5 {
  def simulateMagazine(input: mutable.StringBuilder, rules: Map[String, List[String]]): Unit = {
    val buffer = new mutable.StringBuilder
    while (true) {
      printf("Входная строка: %s\tСодержимое магазина: %s\n", input, buffer)
      if (!isMatchFound(buffer, rules)) {
        if (input.isEmpty) {
          return
        }
        buffer += input.head
        input.deleteCharAt(0)
      }
    }
  }

  private def isMatchFound(buffer: mutable.StringBuilder, rules: Map[String, List[String]]): Boolean = {
    var matched = false
    val rulesMatchBuffer: Option[(String, Option[String])] = rules
      .map { case (key, value) =>
        key -> value.filter(buffer.nonEmpty && buffer.contains(_))
          .maxByOption(_.length)
      }
      .filter(_._2.isDefined)
      .maxByOption { case (_, value) => value.get.length }
    if (rulesMatchBuffer.nonEmpty && rulesMatchBuffer.get._2.nonEmpty) {
      val newBuffer = buffer.toString().replace(rulesMatchBuffer.get._2.get, rulesMatchBuffer.get._1)
      buffer.clear()
      buffer ++ newBuffer
      matched = true
    }
    matched
  }
}